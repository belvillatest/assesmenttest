Belvilla Assessment
Phpunit testing using Selenium2TestCase

1. Go to this web address:

    https://bitbucket.org/belvillatest/assesmenttest/admin

  Here is the git repository where the assessment is stored. 
  You should see this exact document.

2. Go to this web address:

    https://codeanywhere.com/editor/
  
  use these credentials:

  username: belvilla@mail.com
  password: qwerty12345

  Here is where you will complete your assessment.
  This document is also present as README.md
  
3. Make sure that the command line terminal is open in the lower part of the screen, if not right click on the label on the right named "assessmentTest" and select SSH Terminal

4. Make sure you have all the updated dependencies. Type in SSH terminal: 

    composer update
    
    Don't worry about the Warning. this is a development machine.

5. Try to run the pre-existent test typing in SSH terminal: 

    vendor/bin/phpunit test/

6. Check the result on www.browserstack.com 
  
    username: belvilla@mail.com
    password: qwerty12345
   
   Results are under the menu "Automate". You have about 90 minutes of automate testing. 
   This doesn't mean that you have to complete the test in 90 minuts but those are referring to the "actual" time you can use browserstack
   There should be enough for this assessment, but if you run out, please feel free to contact us, we will create another account for you.
   
7. Modify the file test/WebTest.php and create the remaining 3 tests: 

  * checkSearchForRegionFromSearchBar
  * checkPriceFromListerIsWithinRange
  * checkUserPostCode
  
  You can run the test as many times as you want typing in SSH terminal: 
    
    vendor/bin/phpunit test/
   
8. Once you are confident that a test is working, you can save time and maked as skipped. This will prevent the test to run again.
  To do so add these 3 line of code at the begininnig of a test
    
    $this->markTestSkipped(
			'This test is working and we don\'t need to run it for now.'
		);

  Don't forget to remove these lines before submitting the test!

9. Once done, commit the results to bitbucket. Type in SSH terminal these 2 git commands:
    
    git commit -a -m "commit"
    git push origin master


Good Luck!

