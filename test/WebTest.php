<?php

require 'vendor/autoload.php';

define('BROWSERSTACK_USER', 'belvillatest1');
define('BROWSERSTACK_KEY', 'wSHDqmB9CizZseoiq1Na');
class WebTest extends PHPUnit_Extensions_Selenium2TestCase
{
	public static $browsers = array(
		array(
			'browserName' => 'chrome',
			'host' => 'hub.browserstack.com',
			'port' => 80,
			'desiredCapabilities' => array(
				'version' => '30',
				'browserstack.user' => BROWSERSTACK_USER,
				'browserstack.key' => BROWSERSTACK_KEY,
				'os' => 'OS X',
				'os_version' => 'Mountain Lion'		
			)
		),

	);   
	protected function setUp()
	{
		parent::setUp();
		$this->setBrowserUrl('http://www.example.com/');
	}

	/**
	 * Function that wait until the a page completely load
	 *
	 * DO NOT MODIFY
	 *
	 * Wait a maximum of five seconds for the logo to appear
	 *
	 * @return Boolean
	 */
	public function waitForPageToLoad(){
		$this->waitUntil(function() {
			if ($this->byId("logo")) {
				return true;
			}
			return null;
		}, 5000);
	}

	/**
	 * Function that wait until the search page completely load
	 * To be used only on the search page first load or after every change in the filters
	 *
	 * DO NOT MODIFY
	 *
	 * After the logo is loaded, wait a maximum of five seconds for the spinner to disappear
	 *
	 * @return Boolean
	 */	
	public function waitForSearchPageToLoad(){
		$this->waitForPageToLoad();
		sleep(2);
		$this->waitUntil(function() {
			if (!($this->byCssSelector( 'div#loading-search-result' )->displayed())) {
				return true;
			}
			return null;
		}, 5000);
	}

	/**
	 * Function that can initiate a search for a specific term
	 *
	 * DO NOT MODIFY
	 *
	 * Select the searchBar, click on it, clear the existing contents, Type the message and press enter
	 *
	 * @param string $searchTerm
	 */	
	public function searchForTerm($searchTerm) {
		// find search field by its id
		$search = $this->byCssSelector('#keyword');
		$search->click();
		$search->clear();
		// typing into field
		$this->keys($searchTerm);
		// pressing "Enter"
		$this->keys(WebDriverKeys::ENTER);
	}
	
	/**
	 * Function that returns the number or results of a search
	 *
	 * DO NOT MODIFY
	 *
	 * Check the lister result container and count how many lister item are in there
	 *
	 * @return int
	 */
	public function getNumberOfListerItems() {
		$resultList = $this->byCssSelector('div#listitem-container');
		$resultElements = $resultList->elements($this->using('css selector')->value('div.listeritem'));
		return count($resultElements);
	}

	/**
	 * Function that return the element in the position of the lister
	 *
	 * DO NOT MODIFY
	 *
	 * @param int $elementNumber
	 * @return PHPUnit_Extensions_Selenium2TestCase_Element
	 */
	public function getNElement($elementNumber)
	{
		$resultList = $this->byCssSelector('div#listitem-container');
		$resultElements = $resultList->elements($this->using('css selector')->value('div.listeritem'));
		return $resultElements[$elementNumber-1];
	}
	/**
	 * Function that returns house code of a specific element of the search result
	 *
	 * DO NOT MODIFY
	 *
	 * Check the lister result and return the house code of element in the selected position
	 *
	 * @param int $elementNumber
	 * @return String
	 */
	public function getHouseCodeFromLister($elementNumber) {
			return $element = $this->getNElement($elementNumber)->attribute('data-pid');
	}
	
	/**
	 * Test that assert that the homepage have the expected title
	 *
	 * Going to Homepage, Assert title contains the expected words
	 *
	 * @test
	 */	
	public function checkForHomePageTitle()
	{
		$title = 'Topic Travel';
		//sample of a simple working test
		$this->url('http://www.topictravel.nl/');
		$this->assertContains($title, $this->title());
	}

	/**
	 * Test that assert that if searching for a specific house code, the result is only the expected house
	 *
	 * Going to Homepage, Search for an house using house code, wait for page to load, assert that there are only one result and the result is the correct house
	 *
	 * @test
	 */	
	public function checkSearchForHouseCodeFromSearchBar()
	{
		$houseCode = 'XX-1234-03';
		//sample of a simple working test
		$this->url('http://www.topictravel.nl/');
		$this->searchForTerm($houseCode);
		$this->waitForSearchPageToLoad();
		$this->assertEquals('1', $this->getNumberOfListerItems());
		$this->assertEquals($houseCode, $this->getHouseCodeFromLister(1));
	}	
	
	/**
	 * Test that assert that if searching for a region, the returned results are all in the expected region
	 *
	 * Going to Homepage, Search for region term, wait for page to load, assert that the results are all in the expected region
	 *
	 * @test
	 */	
	public function checkSearchForRegionFromSearchBar()
	{
		$region = 'Toscane';
		// Remove these lines and complete the test
		$this->markTestIncomplete(
			'checkSearchForRegionFromSearchBar test has not been implemented yet.'
		);
	}	
	
	/**
	 * Test that assert that if changing the minimum and maximum price in a list, the returned results are all in the expected price range
	 *
	 * Going to Homepage, click on 'Vakantiehuizen', wait for the seach page to load, set the minum and the maximum price, Assert that the prices are all in within the expected range
	 *
	 * @test
	 */	
	public function checkPriceFromListerIsWithinRange()
	{
		$minPrice = 1000;
		$maxPrice = 1200;
		// Remove these lines and complete the test
		$this->markTestIncomplete(
			'checkPriceFromListerIsWithinRange test has not been implemented yet.'
		);
	}	

	/**
	 * Test that assert that the provided user has the expected postcode saved in his details
	 *
	 * Going to Homepage, click on my account, login, select 'Mijn gegevens' and assess the postcode is the one provided
	 *
	 * @test
	 */	
	public function checkUserPostCode()
	{
		$username = 'belvilla@mail.com';
		$password = 'qwerty12345';
		$postcode = '5611DD';
		// Remove these lines and complete the test
		$this->markTestIncomplete(
			'checkPriceFromListerIsWithinRange test has not been implemented yet.'
		);
	}	
	
}
?>